import request from 'request';
import express from 'express';
import rp from 'request-promise-native';
import _ from 'lodash';
import createError from 'http-errors';

const router = express.Router();

const sosHost = process.env.SOS_HOST || 'localhost:5000';
const sosURL = `http://${sosHost}`;
const controllerHost = process.env.CONTROLLER_HOST || 'localhost:8000';
const controllerURL = `http://${controllerHost}`;


const formats = ['mkv', 'mp4', 'mov', 'avi', 'm4v', 'mpeg', 'mpg', 'wmv'];

const getBucketInfo = (bucket) => {
  request.get(`http://localhost:5000/${bucket}?list`, (err, resp, body) => {
    if (!err && resp.statusCode === 200) {
      const info = JSON.parse(body);
      console.log('body:', info);
    }
  });
};

const filterVideos = (object) => {
  for (let i = 0; i < formats.length; i += 1) {
    if (object.name.endsWith(formats[i])) {
      return true;
    }
  }
  return false;
};

const filterGifs = object => !!object.name.endsWith('.gif');

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', { title: 'Express' });
});

router.get('/:bucket/videos', async (req, res, next) => {
  const { bucket } = req.params;
  getBucketInfo(bucket);

  const uri = `${sosURL}/${bucket}?list`;
  console.log(bucket, uri);

  const options = {
    uri,
    method: 'GET',
    json: true,
  };

  const response = (await rp(options).catch(() => null));
  if (response === null) {
    next(createError(404));
    return;
  }
  const objects = response.objects.filter(filterVideos).map((obj) => {
    console.log(obj);
    return obj.name;
  });

  res.render('videos', {
    title: 'Videos',
    bucket,
    objects,
    url: sosURL,
  });
});

router.get('/:bucket/display', async (req, res, next) => {
  const { bucket } = req.params;
  getBucketInfo(bucket);

  const uri = `${controllerURL}/gifs/${bucket}`;
  console.log(bucket, uri);

  const options = {
    uri,
    method: 'GET',
    json: true,
  };

  const result = (await rp(options).then(r => r).catch(() => null));
  if (result === null) {
    next(createError(404));
    return;
  }
  const { gifs } = result;
  const chunked = _.chunk(gifs, 3);
  console.log(gifs);

  res.render('display', {
    title: 'Display',
    bucket,
    gifs: chunked,
    gifs_raw: gifs,
    url: sosURL,
  });
});

module.exports = router;
