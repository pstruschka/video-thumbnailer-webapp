import express from 'express';
import rp from 'request-promise-native';
import createError from 'http-errors';

const router = express.Router();

const sosHost = process.env.SOS_HOST || 'localhost:5000';
const sosURL = `http://${sosHost}`;
const controllerHost = process.env.CONTROLLER_HOST || 'localhost:8000';
const controllerURL = `http://${controllerHost}`;

router.post('/:bucket/', async (req, res) => {
  const { bucket } = req.params;

  const options = {
    method: 'POST',
    json: true,
    uri: `${controllerURL}/gifs/${bucket}?all`,
  };
  await rp(options);

  res.send('Ok');
});

router.delete('/:bucket/', async (req, res) => {
  const { bucket } = req.params;

  const options = {
    uri: `${controllerURL}/gifs/${bucket}`,
    method: 'GET',
    json: true,
  };

  const result = (await rp(options).then(r => r).catch(() => null));
  if (result === null) {
    res.sendStatus(400);
  }
  const { gifs } = result;

  for (let i = 0; i < gifs.length; i += 1) {
    const object = gifs[i].gif;
    const opt = {
      method: 'DELETE',
      json: true,
      uri: `${sosURL}/${bucket}/${object}?delete`,
    };
    rp(opt);
  }

  res.sendStatus(200);
});

router.post('/:bucket/:object', async (req, res) => {
  const { bucket, object } = req.params;

  const options = {
    method: 'POST',
    body: {
      object,
      obj: `${object}.gif`,
    },
    json: true,
    uri: `${controllerURL}/gifs/${bucket}`,
  };
  await rp(options);

  res.sendStatus(200);
});

router.delete('/:bucket/:object', async (req, res) => {
  const { bucket, object } = req.params;

  const options = {
    method: 'DELETE',
    json: true,
    uri: `${sosURL}/${bucket}/${object}?delete`,
  };
  await rp(options).catch();

  res.sendStatus(200);
});


module.exports = router;
