function makeGif(bucket, object) {
	if (object === undefined) {
    var req = new XMLHttpRequest();
    req.open('POST', '/api/' + bucket);
    req.send();
    $('#content p')
      .html('All gif jobs started');
	} else {
    var req = new XMLHttpRequest();
    req.open('POST', '/api/' + bucket + '/' + object);
    req.send();
    $('#content p')
      .html(object + ' gif job started');
  }
};
