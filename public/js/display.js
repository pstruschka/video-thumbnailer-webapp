function deleteGif(bucket, object) {
  var req = new XMLHttpRequest();
  req.open('DELETE', '/api/' + bucket + '/' + object);
  req.send();
  $('#content p')
    .html(object + ' gif deleted');
}

function deleteAllGif(bucket, gifs) {
  console.log(gifs);
  var req = new XMLHttpRequest();
  req.open('DELETE', '/api/' + bucket);
  req.send();
  $('#content p')
    .html(' All gifs deleted');
}
